var request = require('request');
var fs = require('fs');

var reddit = require('redwrap');
reddit
.r('getmotivated')
.top().from('month')
.limit(20, function(err, data, res){
  data.data.children.forEach(function(item){
    if(item.data.link_flair_css_class && item.data.link_flair_css_class == 'image') {
      var ext = item.data.url.substr(item.data.url.length - 4);
      var fsExt = (ext.match(/\./i)) ? ext : '.jpg';
      request(item.data.url).pipe(fs.createWriteStream(item.data.id + fsExt));
    }
  });
});
